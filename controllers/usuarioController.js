const { findOne } = require('../models/usuario');
let Usuario = require('../models/usuario');

async function saveUsuario(req, res) {
    
    const user = await Usuario.findOne({
        'correo' : req.body.correo
    });

    if (user != null) {

        res.status(403).send({message: 'User already registered'})

    } else {
        
        let nuevoUsuario = new Usuario(req.body);

        nuevoUsuario.save((err, result) => {            
    
            if (err) {
                res.status(500).send({message:err});
            } else {
                res.status(200).send({message:'success'});
    
            }        
        });
    }


}

function eliminarUsuario(req, res) {

    let usuario = req.params.id

    Usuario.findByIdAndDelete(usuario, (err, resul) => {
        if (err) {
            res.status(500).send({message: err})
        } else {
            res.status(200).send({message: resul})
        }
    });
    
}

module.exports = {
    saveUsuario,
    eliminarUsuario
}