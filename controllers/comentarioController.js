let Comentario = require('../models/comentario');

function saveComentario(req, res) {
    
    let nuevoComentario = new Comentario(req.body);

    nuevoComentario.save((err, result) => {

        if (err) {
            res.status(500).send({message:err});
        } else {
            res.status(200).send({message:result});

        }        
    });
}

function showComentarios(req, res) {

    let id = req.params.id;
    let comentario = Comentario.find({
        "id anuncio" : id
    });

    comentario.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            //console.log("resul");
            res.status(200).send( result );
        }
    });
}

function deleteComentario(req, res) {

    let id = req.params.id;

    Comentario.findOneAndDelete({'id comentario': id}, (err,result) => {
        
        if (err) {
        
            res.status(500).send({message:err})

        } else {
        
            res.status(500).send({message:'success'})
            
        }
    })
}
module.exports = {
    saveComentario,
    showComentarios,
    deleteComentario
}