const Anuncio = require('../models/anuncio');
const Usuario = require('../models/usuario');

function saveAnuncio(req, res) {
    
    let nuevoAnuncio = new Anuncio(req.body);

    nuevoAnuncio.save((err, resul) => {
        
        if (err) {
            res.status(500).send({message:err});
        } else {
            res.status(200).send({message:resul});
        }
    });

}

function anunciospublicados(req, res) {

    let id = req.params.id;
    let anuncios = Anuncio.find({
        "id usuario vendedor" : id
    });

    anuncios.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });
}

function anuncioId(req, res) {

    const id = req.params.id

    Anuncio.findById(id, (err, resul) => {
        
        if (err) {

            res.status(505).send({message:"Server error, id no encontrado"});
            
        } else {
            //console.log(resul);
            res.status(200).send(resul);
            
        }

    })
    
}

function anuncioVendido(req, res) {

    let anuncio = req.params.id;    
    
    Anuncio.findOneAndUpdate({"id anuncio" :anuncio}, {"estado" : false},{new:true},(err,result) => {
        if (err) {
            res.status(500).send({message:err});
            console.log(err);
        } else {
            res.status(200).send(result);
        }
    });
}

function anuncioComprado(req, res){

    let id = {"id anuncio" : req.params.id}

    Anuncio.findOne(id,(err, result) => {
        
        if (err) {
        
            res.status(500).send({message: err});
        
        } else {

            Usuario.findOne({'id usuario vendedor': result['id usuario vendedor']},(erro, resulta) => {
                
                if (erro) {
                    
                    res.status(500).send({message:erro});
                    
                } else if(result['id usuario vendedor'] === resulta.correo) {
                    
                    res.status(200).send({
                        
                        'Nombre' : resulta.nombre,
                        'Correo' : resulta.correo,
                        'Telefono' : resulta.telefono,
                        'Reputacion' : resulta.puntos
                        
                    })
                } else{
                    res.status(500).send({message: 'Server error'})
                }                
            });
            }
    });

}

function buscarAnuncio(req, res) {
    
    let busqueda = req.params.search;

    let queryParam = {};

    if (busqueda) {

        queryParam={
            $or : [
                {"titulo": {$regex: busqueda, $options: 'i'}},
                {"categoria": {$regex: busqueda, $options: 'i'}}/*,
                {"precio": {$regex: busqueda, $options: 'i'}}*/,
                {"descripcion": {$regex: busqueda, $options: 'i'}},
                {"ubicacion": {$regex: busqueda, $options: 'i'}}
            ]
        };
    }
    //console.log({Busqueda : queryParam});

    query = Anuncio.find(queryParam).sort('createdAt')

    query.exec((err, resul) => {
        
        if (err) {
            console.log({error : err});

            res.status(500).send({message: err});
        
        } else {
            
            res.status(200).send(resul);
        }
    });
}


module.exports = {
    saveAnuncio,
    anunciospublicados,
    anuncioVendido,
    anuncioComprado,
    buscarAnuncio,
    anuncioId
}
