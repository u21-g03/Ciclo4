const Deseo = require('../models/deseo');
let Desep = require('../models/deseo');

function saveDeseo(req, res) {
    
    let nuevoDeseo = new Deseo(req.body);

    nuevoDeseo.save((err, result) => {

        if (err) {
            res.status(500).send({message:err});
        } else {
            res.status(200).send({message:result});

        }        
    });
}

function verDeseos(req, res) {

    let id = req.params.id;
    let deseos = Deseo.find({
        "id usuario comprador" : id
    });

    deseos.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });
}

function deseoTerminado(req, res) {

    let deseo = req.params.id;    
    
    Deseo.findOneAndUpdate({"id Deseo" :deseo}, {"estado" : false},{new:true},(err,result) => {
        
        if (err) {

            res.status(500).send({message:err});
            
        } else {
        
            res.status(200).send(result);
        
        }
    });
}

module.exports = {
    saveDeseo,
    verDeseos,
    deseoTerminado
}