const jwt = require('jsonwebtoken');
const Usuario = require('../models/usuario');
const bcrypt = require("bcrypt");

async function login(req, res) {
    
    const usuario = await Usuario.findOne({
        "correo": req.body.correo
    });

    if (usuario == null) {
        res.status(403).send({message: 'Empty credentials'});
        return;

    } else if (usuario.correo==req.body.correo) {

        //genera un salt con el formato requerido y lo usa para cifrar la contraseña recibida
        let salt = bcrypt.genSaltSync(10);
        const hashedPass = await bcrypt.hash( req.body.contrasena, salt);    

        //Compara la contraseña cifrada con la de la db
        const validPassword = await bcrypt.compare( usuario.contrasena, hashedPass);

        if ( !validPassword ) {
            res.status(400).json({ message: "Incorrect Password" });
            return;
        }

        let token = await new Promise((resolve, reject) => {
            
            jwt.sign(usuario.toJSON(), 'SeedForPasswordToken',{ expiresIn: '1800s' }, (err, token) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(token);
                }
            });
        });

        res.status(200).send({token:token, message:'success'});
        return;
    } else {
        res.status(403).send({message: "Usuario no encontrado"});
        return;
    }
}

function verifyToken(req, res, next) {

    const requestHeader = req.headers['authorization'];

    if (typeof requestHeader !== 'undefined') {
        const tokenUser = requestHeader.split(" ")[1];

        jwt.verify(tokenUser, 'SeedForPasswordToken', (err, payload) => {

            if (err) {
                console.log(err);
                res.status(403).send({
                    error: "Provided token is invalid"
                });

            } else {
                req.data = payload;
                //console.log(req.data.nombre);

                next();
                
            }
        });

    } else {
        res.status(403).send({
            error: "Missing token ",
            message : "No"
        });
    }

}
module.exports = {
    login
//    ,test,
    ,verifyToken
}