//Imports
const {Router} = require('express');
const testController = require('../controllers/testController');
const usuarioController = require('../controllers/usuarioController');
const loginController = require('../controllers/loginController');
const anuncioController = require('../controllers/anuncioController');
const deseoController = require('../controllers/deseoController');
const comentarioController = require('../controllers/comentarioController');


let router= Router();

//test

router.post('/', loginController.verifyToken, testController.saveTest);

//Anuncio

router.get('/anuncios/:id', anuncioController.anuncioId);
router.post('/usuario/anuncio/nuevo', anuncioController.saveAnuncio);
router.put('/usuario/:id/vendido', anuncioController.anuncioVendido);
router.get('/anuncio/:id/comprado', anuncioController.anuncioComprado);
router.get('/anuncio/buscar/:search?', anuncioController.buscarAnuncio);
router.get('/inicio', anuncioController.buscarAnuncio);

//Usuario

router.get('/:id/anuncios', anuncioController.anunciospublicados);
router.post('/auth', loginController.verifyToken)
router.post('/usuario/nuevo', usuarioController.saveUsuario);
router.post('/usuario/login', loginController.login);
router.delete('/usuario/:id/eliminar', usuarioController.eliminarUsuario);

//Deseo

router.post('/usuario/deseo/nuevo', deseoController.saveDeseo);
router.get('/usuario/:id/deseos', deseoController.verDeseos);
router.put('/usuario/deseo/:id/terminar', deseoController.deseoTerminado);

//Comentario

router.post('/usuario/comentario/nuevo', comentarioController.saveComentario );
router.get('/anuncio/:id/comentario', comentarioController.showComentarios);
router.delete('/anuncio/comentario/:id/borrar', comentarioController.deleteComentario)

//Exports
module.exports=router;