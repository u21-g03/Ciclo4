const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const usuarioSchema = new Schema ({

    "nombre" : { type: String, required: true },
    "cedula": { type: String, required: true, index: true, unique: true  },
    "correo": { type: String, required: true, index: true, unique: true  }, 
    "contrasena": { type: String, required: true },
    "telefono":{ type: String, required: true, unique: true },
    "articulos publicados": { type: Array, required: true , default: [] },
    "articulos vendidos": { type: Array, required: true, default: [] },
    "puntos": { type: Number, required: true, default: 0 },
    "deseos" : { type: Array, required: true, default: [] },
    "estado" : { type: Boolean, required: true, default: true  }/*,
    "creado" : { type: Date, required: true, default: Date.now }
*/
    },    
    { 
        timestamps: true 
    });


const Usuario = mongoose.model('usuarios', usuarioSchema );

module.exports = Usuario;