const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const deseoSchema = new Schema ({

    "titulo": { type: String, required: true },
    "id Deseo" : { type: String, required: true, index: true, unique: true  },
    "id usuario comprador": { type: String, required: true, index: true },
    "cantidad": { type: Number, required: false },
    "precio": { type: Number, required: false }, 
    "estado": { type: Boolean, required: true, default: true }, 
    "ubicacion":{ type: Array, required: false },
    "categoria": { type: Array, required: false },
    "tipo de entrega": { type: String, required: false }/*,
    "creado" : { type: Date, required: true, default: Date.now }
*/
    },
    { 
        timestamps: true 
    }
    );


const Deseo = mongoose.model('deseos', deseoSchema );

module.exports = Deseo;