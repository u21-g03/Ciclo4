const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const comentarioSchema = new Schema ({

    "id comentario": { type: String, required: true, index: true, unique: true  },
    "id anuncio": { type: String, required: true, index: true },
    "id usuario" : { type: String, required: true },
    "contenido": { type: String, required: false },

    },
    { 
        timestamps: true 
    }
    );


const Comentario = mongoose.model('comentarios', comentarioSchema );

module.exports = Comentario;