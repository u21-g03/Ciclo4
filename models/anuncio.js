const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const anuncioSchema = new Schema ({

    "titulo" : { type: String, required: true, index: true },
    "descripcion": { type: String, required: true },
    "tipo de entrega": { type: String, required: true }, 
    "id anuncio": { type: String, required: true, index: true, unique: true  }, 
    "categoria":{ type: String, required: true },
    "ubicacion": { type: Array, required: true },
    "id usuario vendedor": { type: String, required: true },
    "id usuario comprador": { type: String, required: true },
    "precio" : { type: Number, required: true },
    "cantidad" : { type: Number, required: true },
    "foto" : { type: Array, required: true },
    "estado" : { type: Boolean, required: true }/*,
    "creado" : { type: Date, required: true, default: Date.now }
*/
    },
    { 
        timestamps: true 
    }
    );


const Anuncio = mongoose.model('anuncios', anuncioSchema );

module.exports = Anuncio;